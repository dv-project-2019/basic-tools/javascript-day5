function loadAllStudents() {
    var responseData = null;
    var data = fetch('https://dv-student-backend-2019.appspot.com/students')
        .then((response) => {
            console.log(response);
            return response.json();
        }).then((json) => {
            responseData = json;
            var resultElement = document.getElementById('result');
            //null is will set to new paragraph after , in json
            //number is set space in the front text
            resultElement.innerHTML = JSON.stringify(json, null, 2);
        })
}

async function loadAllStudentAsync() {
    let response = await fetch('https://dv-student-backend-2019.appspot.com/students');
    let data = await response.json();
    var resultElement = document.getElementById('result');
    //resultElement.innerHTML = JSON.stringify(data, null, 2);
    return data;
}

function createResultTable(data) {
    let resultElement = document.getElementById('resultTable');
    let tableNode = document.createElement('table');
    resultElement.appendChild(tableNode);
    tableNode.setAttribute('class', 'table');

    //create the table
    let tableHeadNode = document.createElement('thead');
    tableNode.appendChild(tableHeadNode);

    var tableRowNode = document.createElement('tr');
    tableHeadNode.appendChild(tableRowNode);

    var tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = '#';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'studentId';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'name';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'surname';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'gpa';
    tableHeadNode.appendChild(tableHeaderNode);

    tableHeaderNode = document.createElement('th');
    tableHeaderNode.setAttribute('scope', 'col');
    tableHeaderNode.innerText = 'image';
    tableHeadNode.appendChild(tableHeaderNode);

    console.log(data);
    data.then((json) => {
        //add data
        for (let i = 0; i < json.length; i++) {
            var currentData = json[i];
            var dataRow = document.createElement('tr');
            tableNode.appendChild(dataRow);

            var dataFirstColumnNode = document.createElement('th');
            dataFirstColumnNode.setAttribute('scope', 'row');
            dataFirstColumnNode.innerText = currentData['id'];
            dataRow.appendChild(dataFirstColumnNode);

            var columnNode = null;
            columnNode = document.createElement('td');
            columnNode.innerText = currentData['studentId'];
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['name'];
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['surname'];
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            columnNode.innerText = currentData['gpa'];
            dataRow.appendChild(columnNode);

            columnNode = document.createElement('td');
            var imageNode = document.createElement('img');
            imageNode.setAttribute('src', currentData['image']);
            imageNode.style.width = '200px';
            imageNode.style.height = '200px';
            dataRow.appendChild(imageNode);

        }
    })
}

async function loadOneStudent() {
    let studentId = document.getElementById('queryId').value;
    console.log(studentId);
    if (studentId != '' && studentId != null) {
        let response = await fetch('https://dv-student-backend-2019.appspot.com/students/' + studentId)
        let data = await response.json();
        return data;
    }
}

function showStudentDataById(data) {
    let studentbyId = document.getElementById('studentById');
    studentbyId.innerHTML = "";
    console.log(data);
    data.then((json) => {
        var currentData = json;

        var rowNode1 = document.createElement('div');
        rowNode1.setAttribute('class', 'row justify-content-center');
        var colNode1 = document.createElement('div');
        colNode1.setAttribute('class', 'col');
        var imageNode = document.createElement('img');
        imageNode.setAttribute('src', currentData['image']);
        imageNode.style.width = '200px';
        imageNode.style.height = '200px';
        colNode1.appendChild(imageNode);
        rowNode1.appendChild(colNode1);
        studentbyId.appendChild(rowNode1);

        var rowNode2 = document.createElement('div');
        rowNode2.setAttribute('class', 'row justify-content-center');
        var colNode21 = document.createElement('div');
        colNode21.setAttribute('class', 'col-2');
        var stuId_topic = document.createElement('p');
        stuId_topic.innerText = 'Student id: ';
        colNode21.appendChild(stuId_topic);

        var colNode22 = document.createElement('div');
        colNode22.setAttribute('class', 'col-2');
        var id = document.createElement('p');
        id.innerText = currentData['studentId'];
        colNode22.appendChild(id);
        rowNode2.appendChild(colNode21);
        rowNode2.appendChild(colNode22);
        studentbyId.appendChild(rowNode2);

        var rowNode3 = document.createElement('div');
        rowNode3.setAttribute('class', 'row justify-content-center');
        var colNode31 = document.createElement('div');
        colNode31.setAttribute('class', 'col-2');
        var name_topic = document.createElement('p');
        name_topic.innerText = 'Name: ';
        colNode31.appendChild(name_topic);

        var colNode32 = document.createElement('div');
        colNode32.setAttribute('class', 'col-2');
        var name = document.createElement('p');
        name.innerText = currentData['name'];
        colNode32.appendChild(name);
        rowNode3.appendChild(colNode31);
        rowNode3.appendChild(colNode32);
        studentbyId.appendChild(rowNode3);

        var rowNode4 = document.createElement('div');
        rowNode4.setAttribute('class', 'row justify-content-center');
        var colNode41 = document.createElement('div');
        colNode41.setAttribute('class', 'col-2');
        var surname_topic = document.createElement('p');
        surname_topic.innerText = 'Surname: ';
        colNode41.appendChild(surname_topic);

        var colNode42 = document.createElement('div');
        colNode42.setAttribute('class', 'col-2');
        var surname = document.createElement('p');
        surname.innerText = currentData['surname'];
        colNode42.appendChild(surname);
        rowNode4.appendChild(colNode41);
        rowNode4.appendChild(colNode42);
        studentbyId.appendChild(rowNode4);

        var rowNode5 = document.createElement('div');
        rowNode5.setAttribute('class', 'row justify-content-center');
        var colNode51 = document.createElement('div');
        colNode51.setAttribute('class', 'col-2');
        var gpa_topic = document.createElement('p');
        gpa_topic.innerText = 'Gpa: ';
        colNode51.appendChild(gpa_topic);

        var colNode52 = document.createElement('div');
        colNode52.setAttribute('class', 'col-2');
        var gpa = document.createElement('p');
        gpa.innerText = currentData['gpa'];
        colNode52.appendChild(gpa);
        rowNode5.appendChild(colNode51);
        rowNode5.appendChild(colNode52);
        studentbyId.appendChild(rowNode5);

    })


}
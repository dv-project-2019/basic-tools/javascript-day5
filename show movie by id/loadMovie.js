async function loadMovieByName() {
    let movieName = document.getElementById('movieName').value;
    console.log(movieName);
    if (movieName != '' && movieName != null) {
        let response = await fetch('https://dv-excercise-backend.appspot.com/movies/' + movieName)
        let data = await response.json();
        console.log(response);
        return data;
    }
}

function showMovieDataByName(data) {
    let showMovie = document.getElementById('showMovie');
    showMovie.innerHTML = "";
    console.log(data);
    data.then((json) => {
        for (var i = 0; i < json.length; i++) {
            var currentData = json[i];

            var rowNode1 = document.createElement('div');
            rowNode1.setAttribute('class', 'row justify-content-center');
            var colNode1 = document.createElement('div');
            colNode1.setAttribute('class', 'col');
            var img_block = document.createElement('div');
            img_block.setAttribute('class', 'img_block');
            img_block.style.margin = '20px auto';
            img_block.style.textAlign = 'center';
            var imageNode = document.createElement('img');
            imageNode.setAttribute('src', currentData['imageUrl']);
            imageNode.style.width = '280px';
            imageNode.style.height = 'auto';
            img_block.appendChild(imageNode);
            colNode1.appendChild(img_block);
            rowNode1.appendChild(colNode1);
            showMovie.appendChild(rowNode1);

            var rowNode2 = document.createElement('div');
            rowNode2.setAttribute('class', 'row justify-content-center');
            var colNode21 = document.createElement('div');
            colNode21.setAttribute('class', 'col-2');
            var name_topic = document.createElement('p');
            name_topic.innerText = 'ชื่อภาพยนตร์: ';
            colNode21.appendChild(name_topic);

            var colNode22 = document.createElement('div');
            colNode22.setAttribute('class', 'col-4');
            var name = document.createElement('p');
            name.innerText = currentData['name'];
            colNode22.appendChild(name);
            rowNode2.appendChild(colNode21);
            rowNode2.appendChild(colNode22);
            showMovie.appendChild(rowNode2);

            var rowNode3 = document.createElement('div');
            rowNode3.setAttribute('class', 'row justify-content-center');
            var colNode31 = document.createElement('div');
            colNode31.setAttribute('class', 'col-2');
            var des_topic = document.createElement('p');
            des_topic.innerText = 'บทย่อ: ';
            colNode31.appendChild(des_topic);

            var colNode32 = document.createElement('div');
            colNode32.setAttribute('class', 'col-4');
            var synopsis = document.createElement('p');
            synopsis.innerText = currentData['synopsis'];
            colNode32.appendChild(synopsis);
            rowNode3.appendChild(colNode31);
            rowNode3.appendChild(colNode32);
            showMovie.appendChild(rowNode3);
        }
    })

}